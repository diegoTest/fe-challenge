import "bulma/css/bulma.min.css";
import Navbar from "./components/header/Navbar";
import Footer from "./components/footer/Footer";
import Card from "./components/card/Card";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import CardDetail from "./components/cardDetail/CardDetail";

function App() {
  return (
    <BrowserRouter>

      <Navbar />

      <Routes>
        <Route path="/" element={<Card />} />
        <Route path="/api/quote/:id" element={<CardDetail />} />
      </Routes>
      <Footer />
    </BrowserRouter>

  );
}

export default App;
