import React from "react";
import "./style.css";

export default function Footer() {
  return (
    <footer>
      <ul className="ul-footer">
        <li className="ul-li-footer">
          <a className="footer-a-link" href="/">
            About
          </a>
        </li>
        <li className="ul-li-footer">
          <a className="footer-a-link" href="/">
            Email Preference
          </a>
        </li>
        <li className="ul-li-footer">
          <a className="footer-a-link" href="/">
            Privacy Policy
          </a>
        </li>
        <li className="ul-li-footer">
          <a className="footer-a-link" href="/">
            Terms of use
          </a>
        </li>
        <li className="ul-li-footer">
          <a className="footer-a-link" href="/">
            Contact
          </a>
        </li>
        <li className="ul-li-footer">
          <a className="footer-a-link" href="/">
            Do Not Sell My Info
          </a>
        </li>
      </ul>
    </footer>
  );
}
