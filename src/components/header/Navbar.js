/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";

export default function Navbar() {

  const [isActive, setisActive] = React.useState(false);

  return (
    <nav className="navbar" role="navigation" aria-label="main navigation">
      <div className="navbar-brand">
        <a className="navbar-item" href="/">
          <img src="/images/Likemind.png" alt={"logo"} width="80" height="28" />
        </a>
        <a
          onClick={() => {
            setisActive(!isActive);
          }}
          role="button"
          className={`navbar-burger burger ${isActive ? "is-active" : ""}`}
          aria-label="menu"
          aria-expanded="false"
          data-target="navbarBasicExample"
        >
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </a>
      </div>
      <div
        id="navbarBasicExample"
        className={`navbar-menu ${isActive ? "is-active" : ""}`}
      >
        <div className="navbar-end">
          <a href="/" className="navbar-item">Home</a>
          <a href="/" className="navbar-item">Quotes</a>
          <a href="/" className="navbar-item">Articles</a>
          <a href="/" className="navbar-item">Games</a>
        </div>
      </div>
    </nav>
  );
}
