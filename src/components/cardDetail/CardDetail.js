import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import 'bulma/css/bulma.min.css';
import Moment from 'react-moment'

export default function CardDetail() {
  const { id } = useParams();

  const [isLoading, setIsLoading] = useState(true);
  const [quote, setQuote] = useState(null);

  useEffect(() => {
    fetch(`https://api-challenge-t-diegof.vercel.app/api/quote/${id}`)
      .then((response) => response.json())
      .then((quote) => {
        setQuote(quote);
        setIsLoading(false);
      });
  }, [id]);

  if (isLoading) {
    return (
      <div>
        <progress className="progress is-large is-dark" max="100">60%</progress>
      </div>
    );
  }
  return (

    <div className="container">
      <div className="card">
        <div className="card-image">
          <figure className="desktop-image">
            <img
              src={quote.desktopImage.publicURL}
              alt="desktopImage"
              className="desktop-image"
            ></img>
          </figure>
          <figure className="mobile-image">
            <img
              src={quote.mobileImage.publicURL}
              alt="mobileImage"
              className="mobible-image"
            ></img>
          </figure>

        </div>
        <div className="card-content">
          <p className="title">
            {quote.title}
          </p>

          <div className="content">
            {quote.description}
            <p>
              <Moment format="MMM D, YYYY" withTitle>{quote.activeTs}</Moment>
            </p>
            <p className="subtitle">
              {quote.author.title}
            </p>
          </div>


          <footer className="card-footer">
            <p className="card-footer-item">
              <span>
                <a href="https://www.facebook.com/">
                  <img src="/icons/fb-icon.svg" alt={"fb-icon"} />
                </a>
              </span>
            </p>
            <p className="card-footer-item">
              <span>
                <a href="https://twitter.com/">
                  <img src="/icons/tw-icon.svg" alt={"tw-icon"} />
                </a>
              </span>
            </p>
            <p className="card-footer-item">
              <span>
                <img src="/icons/email-icon.svg" alt={"email-icon"} />
              </span>
            </p>
          </footer>
        </div>
      </div>
    </div >

  );
}

