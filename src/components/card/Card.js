import React, { useEffect, useState } from "react";
import "./style.css";
import { Link } from "react-router-dom";
import Moment from 'react-moment'

export default function Card() {

  const [quotes, setQuotes] = useState([]);
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    fetch(`https://api-challenge-t-diegof.vercel.app/api/quote`)
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setQuotes(result.quotes);
        },

        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )
  }, [])
  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>
      <progress className="progress is-large is-dark" max="100">60%</progress>
    </div>
  } else {

    return (
      <ol className="grid-container">
        {quotes.map((item) => (
          <div key={item._id}>
            <Link to={`api/quote/${item._id}`}>
              <div>
                <img
                  src={item.desktopImage.publicURL}
                  alt="desktopImage"
                  className="desktop-image"
                ></img>
              </div>
              <div>
                <img
                  src={item.mobileImage.publicURL}
                  alt="mobileImage"
                  className="mobile-image"
                ></img>
              </div>
              <div className="message is-small">
                <div className="message-body">
                  <Moment format="MMM D, YYYY" withTitle>{item.activeTs}</Moment>
                </div>
              </div></Link>
          </div>
        ))}
      </ol>
    );
  }
}
