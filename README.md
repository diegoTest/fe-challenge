# Likemind FE Challenge

### Built With

The technologies used to solve the challenge are the following

- [React.js](https://reactjs.org/)
- [Bulma](https://bulma.io/)
- [Sass](https://sass-lang.com/documentation)
- [Vercel](https://vercel.com)

## Installation

Use the following command to install the dependencies

```bash
npm i
```

## Usage

```npm run css-build

# run the project
npm run start

```

## Notes

I started out using css to style components to learn the basics before using a css framework. It is for this reason that the card component uses bulma and the footer uses only css.
Do not save the urls of the api in environment variables so that you can see that I made the backend and did not consume the api that was delivered in the challenge.
